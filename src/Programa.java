import java.util.Scanner;

public class Programa {
	
	public static void main(String[] args) {
	
		Scanner leitor = new Scanner(System.in);
		
		System.out.println("Digite o nome:");
		String nome = leitor.nextLine();
		
		System.out.println("Digite o sobrenome:");
		String sobrenome = leitor.nextLine();
		
		System.out.println(nome + " " + sobrenome); 
		
		leitor.close();
	}
}
